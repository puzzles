package com.github.puzzles.core;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.puzzles.util.Matrices;

public class FlippingPuzzle extends AbstractRectangularPuzzle<Boolean>
		implements Flipable<Point> {

	protected FlippingPuzzle(Boolean[][] puzzle) {
		super(puzzle, Difficulty.PERSONALISED);
	}

	public FlippingPuzzle(int width, int height) {
		// this(makePuzzleHelper(width, height));
		super(makePuzzleHelper(width, height), Difficulty.PERSONALISED);
	}

	public FlippingPuzzle(Difficulty difficulty) {
		this(makePuzzleFromDifficultyHelper(difficulty));
	}

	public FlippingPuzzle(FlippingPuzzle puzzle) {
		super(puzzle);
	}

	private Boolean toggle(Boolean bool) {
		return !bool;
	}

	private static Boolean[][] makePuzzleHelper(int width, int height) {

		Boolean booleanMatrix[][] = new Boolean[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				booleanMatrix[i][j] = false;
			}
		}

		return booleanMatrix;
	}

	private static Boolean[][] makePuzzleFromDifficultyHelper(
			Difficulty difficulty) throws PersonalisedDifficultyException {
		if (difficulty == Difficulty.VERY_EASY) {
			return makePuzzleHelper(3, 3);
		} else if (difficulty == Difficulty.EASY) {
			return makePuzzleHelper(5, 5);
		} else if (difficulty == Difficulty.MEDUIM) {
			return makePuzzleHelper(7, 7);
		} else if (difficulty == Difficulty.HARD) {
			return makePuzzleHelper(9, 9);
		} else if (difficulty == Difficulty.VERY_HARD) {
			return makePuzzleHelper(11, 11);
		}

		throw new PersonalisedDifficultyException();
	}

	@Override
	public Boolean[][] makeCorrectPuzzle() {
		Boolean[][] correctPuzzle = new Boolean[getWidth()][getHeight()];
		Matrices.fill(correctPuzzle, true);
		return correctPuzzle;
	}

	@Override
	public boolean check() {
		return Matrices.equals(getPuzzle(), getCorrectPuzzle());
	}

	@Override
	public void flip(Point cell) {
		flip((int) cell.getX(), (int) cell.getY());
	}

	public void flip(int x, int y) {
		try {
			puzzle[y][x] = toggle(puzzle[y][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new RectangularPuzzleIndexOutOfBoundsException();
		}

		try {
			puzzle[y][x + 1] = toggle(puzzle[y][x + 1]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			puzzle[y][x - 1] = toggle(puzzle[y][x - 1]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			puzzle[y + 1][x] = toggle(puzzle[y + 1][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			puzzle[y - 1][x] = toggle(puzzle[y - 1][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		incrementCounter();
	}

	public Boolean[] getWillFlip(int x, int y){
		List<Boolean> willFlip = new ArrayList<>();
		try {
			willFlip.add(new Boolean(puzzle[y][x]));
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new RectangularPuzzleIndexOutOfBoundsException();
		}

		try {
			willFlip.add(new Boolean(puzzle[y][x + 1]));
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(new Boolean(puzzle[y][x - 1]));
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(new Boolean(puzzle[y + 1][x]));
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(new Boolean(puzzle[y - 1][x]));
		} catch (ArrayIndexOutOfBoundsException ex) {
		}
		
		return (Boolean[])willFlip.toArray(new Boolean[0]);
	}
	
	@Override
	public String toString() {
		return "FlipPuzzle [puzzle=" + Arrays.toString(puzzle) + "]";
	}
}
