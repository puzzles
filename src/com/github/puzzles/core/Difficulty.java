package com.github.puzzles.core;

public enum Difficulty {
	VERY_EASY, EASY, MEDUIM, HARD, VERY_HARD, PERSONALISED
}
