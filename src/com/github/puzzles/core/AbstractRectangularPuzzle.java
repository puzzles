/**
 * Rectangular puzzles should inherit this class.
 */
package com.github.puzzles.core;

import java.util.Arrays;

import com.github.puzzles.util.Matrices;

public abstract class AbstractRectangularPuzzle<T> extends AbstractPuzzle {

	final protected int width;
	final protected int height;
	final protected T puzzle[][];
	final protected T correctPuzzle[][];

	protected AbstractRectangularPuzzle(T[][] puzzle, Difficulty difficulty) {
		super(Matrices.getWidth(puzzle) * Matrices.getHeight(puzzle), 0,
				difficulty);
		this.width = Matrices.getWidth(puzzle);
		this.height = Matrices.getHeight(puzzle);
		this.puzzle = puzzle;
		this.correctPuzzle = makeCorrectPuzzle();
		if(width < 0 || height < 0)
			throw new InvalidPuzzleSizeException();
	}

	protected AbstractRectangularPuzzle(
			AbstractRectangularPuzzle<T> rectangularPuzzle) {
		super(rectangularPuzzle);
		this.width = rectangularPuzzle.width;
		this.height = rectangularPuzzle.height;
		this.puzzle = (T[][]) Matrices.copyOf(rectangularPuzzle.puzzle);
		this.correctPuzzle = makeCorrectPuzzle();
	}

	public T[][] getCorrectPuzzle() {
		return Matrices.copyOf(correctPuzzle);
	}

	/**
	 * Getter.
	 * 
	 * @return the width of the puzzle.
	 */
	public final int getWidth() {
		return width;
	}

	abstract public T[][] makeCorrectPuzzle();

	/**
	 * Getter.
	 * 
	 * @return the height of the puzzle.
	 */
	public final int getHeight() {
		return height;
	}

	/**
	 * Getter.
	 * 
	 * @return the puzzle matrix.
	 */
	public final T[][] getPuzzle() {
		return Matrices.copyOf(puzzle);
		// return puzzle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + height;
		result = prime * result + Arrays.hashCode(puzzle);
		result = prime * result + width;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractRectangularPuzzle other = (AbstractRectangularPuzzle) obj;
		if (height != other.height) {
			return false;
		}
		if (!Arrays.deepEquals(puzzle, other.puzzle)) {
			return false;
		}
		if (width != other.width) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AbstractRectangularPuzzle [width=" + width + ", height="
				+ height + ", puzzle=" + Arrays.toString(puzzle) + "]";
	}
}
