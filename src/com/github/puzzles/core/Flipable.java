package com.github.puzzles.core;


public interface Flipable<T> {
	public void flip(T cell);
}
