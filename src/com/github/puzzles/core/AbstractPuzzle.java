/**
 * All puzzles must inherit this class.
 */
package com.github.puzzles.core;

public abstract class AbstractPuzzle {

	private int size;
	private int counter;
	private Difficulty difficulty;

	protected AbstractPuzzle(int size, int count, Difficulty difficulty) {
		this.size = size;
		this.counter = count;
		this.difficulty = difficulty;
	}

	protected AbstractPuzzle(AbstractPuzzle puzzle) {
		this.size = puzzle.size;
		this.counter = puzzle.counter;
		this.difficulty = puzzle.difficulty;
	}

	/**
	 * Check to see if the puzzle is correct or not.
	 * 
	 * @return true if the puzzle is correct, otherwise false.
	 */
	public abstract boolean check();

	/**
	 * Increment the counter of played times.
	 * 
	 * @return the counter.
	 */
	protected int incrementCounter() {
		return ++counter;
	}

	/**
	 * Decrement the counter of played times.
	 * 
	 * @return the counter.
	 */
	protected int decrementCounter() {
		return --counter;
	}

	/**
	 * Set the counter value.
	 * 
	 * @param counter
	 */
	protected void setCounter(int counter) {
		this.counter = counter;
	}

	protected void reintialiseCounter() {
		this.counter = 0;
	}

	/**
	 * Getter.
	 * 
	 * @return the size of the puzzle.
	 */
	public final int getSize() {
		return size;
	}

	/**
	 * Getter.
	 * 
	 * @return the number of the played times.
	 */
	public final int getCounter() {
		return counter;
	}

	/**
	 * Getter.
	 * 
	 * @return the difficulty of the puzzle.
	 */
	public final Difficulty getDifficulty() {
		return difficulty;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + counter;
		result = prime * result
				+ ((difficulty == null) ? 0 : difficulty.hashCode());
		result = prime * result + size;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AbstractPuzzle [size=" + size + ", counter=" + counter
				+ ", difficulty=" + difficulty + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractPuzzle other = (AbstractPuzzle) obj;
		if (counter != other.counter) {
			return false;
		}
		if (difficulty != other.difficulty) {
			return false;
		}
		if (size != other.size) {
			return false;
		}
		return true;
	}
}
