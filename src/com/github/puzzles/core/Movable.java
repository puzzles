package com.github.puzzles.core;

public interface Movable<T> {
	public void move(T fromIndex, T toIndex);
}
