package com.github.puzzles.core;

import java.awt.Point;
import java.util.Random;

import com.github.puzzles.util.Matrices;

public class SlidingPuzzle extends AbstractRectangularPuzzle<Integer> implements
		Slidable<Point> {

	private static final int MAX_SWAP = 512;

	protected SlidingPuzzle(Integer[][] puzzle) {
		super(puzzle, Difficulty.PERSONALISED);
	}

	public SlidingPuzzle(int width, int height) {
		// this(makeCorrectPuzzleHelper(width, height));
		super(makeCorrectPuzzleHelper(width, height), Difficulty.PERSONALISED);
	}

	public SlidingPuzzle(Difficulty difficulty) {
		this(makePuzzleFromDifficultyHelper(difficulty));
	}

	public SlidingPuzzle(SlidingPuzzle puzzle) {
		super(puzzle);
	}

	private static Integer[][] makeCorrectPuzzleHelper(int width, int height) {
		Integer[][] puzzle = new Integer[width][height];
		for (int i = 0, k = 1; i < width; i++) {
			for (int j = 0; j < height; j++, k++) {
				puzzle[i][j] = k;
			}
		}

		puzzle[width - 1][height - 1] = 0;

		return puzzle;
	}

	private static Integer[][] makePuzzleHelper(int width, int height) {
		Integer[][] puzzle = makeCorrectPuzzleHelper(width, height);
		Random randoms = new Random();

		for (int i = 0; i < MAX_SWAP; i++) {
			Matrices.swap(puzzle, randoms.nextInt(width),
					randoms.nextInt(width), randoms.nextInt(height),
					randoms.nextInt(height));
		}

		return puzzle;
	}

	private static Integer[][] makePuzzleFromDifficultyHelper(
			Difficulty difficulty) throws PersonalisedDifficultyException {
		if (difficulty == Difficulty.VERY_EASY) {
			return makePuzzleHelper(3, 2);
		} else if (difficulty == Difficulty.EASY) {
			return makePuzzleHelper(5, 3);
		} else if (difficulty == Difficulty.MEDUIM) {
			return makePuzzleHelper(5, 5);
		} else if (difficulty == Difficulty.HARD) {
			return makePuzzleHelper(7, 7);
		} else if (difficulty == Difficulty.VERY_HARD) {
			return makePuzzleHelper(11, 11);
		}

		throw new PersonalisedDifficultyException();
	}

	@Override
	public Integer[][] makeCorrectPuzzle() {
		int width = getWidth();
		int height = getHeight();
		Integer[][] correctPuzzle = new Integer[width][height];

		for (int i = 0; i < correctPuzzle.length; i++) {
			for (int j = 0; j < correctPuzzle[i].length; j++) {
				correctPuzzle[i][j] = i * width + j + 1;
			}
		}
		correctPuzzle[width - 1][height - 1] = 0;

		return correctPuzzle;
	}

	@Override
	public boolean check() {
		return Matrices.equals(getPuzzle(), getCorrectPuzzle());
	}

	@Override
	public void slid(Point index) {
		slid((int) index.getX(), (int) index.getY());
	}

	public void slid(int x, int y) {
		if (puzzle[y][x] == 0) {
			throw new RectangularPuzzleIndexOutOfBoundsException();
		}

		try {
			if (puzzle[y - 1][x] == 0) {
				Matrices.swap(puzzle, x, y, x - 1, y);
				incrementCounter();
				return;
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			if (puzzle[y + 1][x] == 0) {
				Matrices.swap(puzzle, x, y, x + 1, y);
				incrementCounter();
				return;
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			if (puzzle[y][x - 1] == 0) {
				Matrices.swap(puzzle, x, y, x, y - 1);
				incrementCounter();
				return;
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			if (puzzle[y][x + 1] == 0) {
				Matrices.swap(puzzle, x, y, x, y + 1);
				incrementCounter();
				return;
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		throw new NoEmptyCaseAdjacentSlidingPuzzleException();
	}
}
