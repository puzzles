package com.github.puzzles.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.github.puzzles.core.FlippingPuzzle;

public class FlippingPuzzleDialog extends JDialog {

	/**
     * 
     */
	private static final long serialVersionUID = 4729972122941718936L;
	private final JPanel contentPanel = new JPanel();
	private JTextField rowsText;
	private JTextField colsText;
	private JLabel rowsNumberMessage;
	private JLabel rowsNotANumberErrorMessage;
	private JLabel colsNumberMessage;
	private JLabel colsNotANumberErrorMessage;
	private JLabel colsNegativeNumberErrorMessage;
	private JLabel rowsNegativeNumberErrorMessage;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { try { FlippingPuzzleDialog
	 * dialog = new FlippingPuzzleDialog();
	 * dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	 * dialog.setVisible(true); } catch (Exception e) { e.printStackTrace(); } }
	 * //
	 */

	/**
	 * Create the dialog.
	 */
	public FlippingPuzzleDialog(final MainWindow mainWindow) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				rowsNumberMessage = new JLabel("Rows number");
				panel.add(rowsNumberMessage);
			}
			{
				rowsText = new JTextField();
				panel.add(rowsText);
				rowsText.setColumns(10);
			}
			rowsNotANumberErrorMessage = new JLabel("* should be a number.");
			panel.add(rowsNotANumberErrorMessage);

			rowsNegativeNumberErrorMessage = new JLabel(
					"* should be a positif value.");
			rowsNegativeNumberErrorMessage.setVisible(false);
			panel.add(rowsNegativeNumberErrorMessage);
			rowsNotANumberErrorMessage.setVisible(false);
		}

		JPanel panel = new JPanel();
		contentPanel.add(panel);
		{
			colsNumberMessage = new JLabel("Cols number");
			panel.add(colsNumberMessage);
		}
		{
			colsText = new JTextField();
			panel.add(colsText);
			colsText.setColumns(10);
		}
		{
			colsNotANumberErrorMessage = new JLabel("* should be a number");
			colsNotANumberErrorMessage.setVisible(false);
			panel.add(colsNotANumberErrorMessage);
		}
		{
			colsNegativeNumberErrorMessage = new JLabel(
					"* should be a positif value.");
			colsNegativeNumberErrorMessage.setVisible(false);
			panel.add(colsNegativeNumberErrorMessage);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						int rows = 0;
						int cols = 0;
						boolean error = false;

						rowsNotANumberErrorMessage.setVisible(false);
						rowsNegativeNumberErrorMessage.setVisible(false);
						colsNotANumberErrorMessage.setVisible(false);
						colsNegativeNumberErrorMessage.setVisible(false);
						try {
							rows = Integer.parseInt(rowsText.getText());
							if (rows <= 0) {
								error = true;
								rowsNegativeNumberErrorMessage.setVisible(true);
							}
						} catch (NumberFormatException e1) {
							rowsNotANumberErrorMessage.setVisible(true);
							error = true;
						}

						try {
							cols = Integer.parseInt(colsText.getText());
							if (cols <= 0) {
								error = true;
								colsNegativeNumberErrorMessage.setVisible(true);
							}
						} catch (NumberFormatException e1) {
							colsNotANumberErrorMessage.setVisible(true);
							error = true;
						}

						if (!error) {
							mainWindow.reset();
							mainWindow.setFlippingPuzzle(new FlippingPuzzle(
									rows, cols));
							// puzzlePanel.add(new
							// FlippingPuzzlePanel(MainWindow.this));
							mainWindow.getPuzzlePanel().add(
									new FlippingPuzzlePanel(mainWindow));
							mainWindow.getFrame().revalidate();
							mainWindow.getFrame().repaint();

							FlippingPuzzleDialog.this.dispose();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						FlippingPuzzleDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public JLabel getColsNegativeNumberErrorMessage() {
		return colsNegativeNumberErrorMessage;
	}

	public JLabel getRowsNegativeNumberErrorMessage() {
		return rowsNegativeNumberErrorMessage;
	}
}
