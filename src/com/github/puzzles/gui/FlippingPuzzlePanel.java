package com.github.puzzles.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.github.puzzles.core.RectangularPuzzleIndexOutOfBoundsException;

public class FlippingPuzzlePanel extends JPanel {
	final private int rows;
	final private int cols;
	private Cell puzzlePanels[][];
	MainWindow mainWindow;

	public class Cell extends AbstractCellPuzzlePanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7439563525463254651L;

		public Cell(int xIndex, int yIndex) {
			super(xIndex, yIndex);
			add(new JLabel("     "));

			addMouseListener(new MouseAdapter() {

				@Override
				public void mouseReleased(MouseEvent e) {
					FlippingPuzzlePanel.this.mainWindow.getFlippingPuzzle()
							.flip(getXIndex(), getYIndex());
					FlippingPuzzlePanel.this.paint();
					count.setText("Count : "
							+ mainWindow.getFlippingPuzzle().getCounter());

					if (mainWindow.getFlippingPuzzle().check()) {
						StringBuffer message = new StringBuffer(
								"Congratz\nYou have won it in "
										+ mainWindow.getFlippingPuzzle()
												.getCounter() + " time");
						if (mainWindow.getFlippingPuzzle().getCounter() > 1)
							message.append('s');
						JOptionPane.showMessageDialog(null, message);
						mainWindow.reset();
					}
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					Cell [] cells = FlippingPuzzlePanel.this.getWillFlip(getXIndex(), getYIndex());
					for(Cell cell : cells)
						cell.setBackground(Color.GREEN);
					
					FlippingPuzzlePanel.this.revalidate();
					FlippingPuzzlePanel.this.repaint();
				}

				@Override
				public void mouseExited(MouseEvent e) {
					FlippingPuzzlePanel.this.paint();
					FlippingPuzzlePanel.this.revalidate();
					FlippingPuzzlePanel.this.repaint();
				}
			});
		}

		public void reset() {
			FlippingPuzzlePanel.this.reset();
		}
	}

	/**
     * 
     */
	private static final long serialVersionUID = 4872124821136999470L;
	private JLabel count;
	
	private Cell[] getWillFlip(int x, int y){
		List<Cell> willFlip = new ArrayList<>();
		try {
			willFlip.add(puzzlePanels[y][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new RectangularPuzzleIndexOutOfBoundsException();
		}

		try {
			willFlip.add(puzzlePanels[y][x + 1]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(puzzlePanels[y][x - 1]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(puzzlePanels[y + 1][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}

		try {
			willFlip.add(puzzlePanels[y - 1][x]);
		} catch (ArrayIndexOutOfBoundsException ex) {
		}
		
		return willFlip.toArray(new Cell[0]);
	}

	/**
	 * Create the panel.
	 */
	public FlippingPuzzlePanel(MainWindow mainWindow) {
		this.rows = mainWindow.getFlippingPuzzle().getWidth();
		this.cols = mainWindow.getFlippingPuzzle().getHeight();
		this.puzzlePanels = new Cell[rows][cols];
		this.mainWindow = mainWindow;

		JPanel panel = new JPanel();
		add(panel);

		count = new JLabel("Count : "
				+ mainWindow.getFlippingPuzzle().getCounter());
		panel.add(count);

		for (int i = 0; i < rows; i++) {
			JPanel rowsPanel = new JPanel();
			for (int j = 0; j < cols; j++) {
				puzzlePanels[i][j] = new Cell(j, i);
				rowsPanel.add(puzzlePanels[i][j]);
			}
			this.mainWindow.getPuzzlePanel().add(rowsPanel);
		}

		paint();
	}

	private void paint() {
		Boolean[][] puzzle = mainWindow.getFlippingPuzzle().getPuzzle();
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				puzzlePanels[i][j]
						.setBackground((puzzle[i][j] == true) ? Color.BLUE
								: Color.RED);
	}

	public void reset() {
		puzzlePanels = null;
		mainWindow.reset();
	}

	public int getRows() {
		return rows;
	}

	public int getCols() {
		return cols;
	}

	public JPanel[][] getPuzzlePanels() {
		return puzzlePanels;
	}
}
