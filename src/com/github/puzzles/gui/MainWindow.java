package com.github.puzzles.gui;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.github.puzzles.core.FlippingPuzzle;
import com.github.puzzles.core.SlidingPuzzle;

public class MainWindow {

	private JFrame frame;
	private FlippingPuzzle flippingPuzzle;
	private SlidingPuzzle sliddingPuzzle;
	private JPanel mainPanel;
	private JPanel puzzlePanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setBounds(0, 0, 800, 600);

		JMenuBar topMenuBar = new JMenuBar();
		frame.setJMenuBar(topMenuBar);

		JMenu menuFile = new JMenu("File");
		topMenuBar.add(menuFile);

		JMenu newPuzzleMenu = new JMenu("New puzzle");
		menuFile.add(newPuzzleMenu);

		JMenuItem flippingPuzzle = new JMenuItem("Flipping Puzzle");
		flippingPuzzle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {

				JDialog flippingPuzzleDialog = new FlippingPuzzleDialog(
						MainWindow.this);
				flippingPuzzleDialog
						.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				flippingPuzzleDialog.setAlwaysOnTop(true);
				flippingPuzzleDialog.setModal(true);
				flippingPuzzleDialog.setVisible(true);

				/*
				 * if(MainWindow.this.flippingPuzzle != null){
				 * puzzlePanel.add(new FlippingPuzzlePanel(MainWindow.this));
				 * 
				 * frame.revalidate(); frame.repaint(); } //
				 */
			}
		});
		newPuzzleMenu.add(flippingPuzzle);

		JMenuItem slidingPuzzle = new JMenuItem("Sliding Puzzle");
		slidingPuzzle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JDialog slidingPuzzleDialog = new SlidingPuzzleDialog(
						MainWindow.this);
				slidingPuzzleDialog
						.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				slidingPuzzleDialog.setAlwaysOnTop(true);
				slidingPuzzleDialog.setModal(true);
				slidingPuzzleDialog.setVisible(true);

			}
		});
		newPuzzleMenu.add(slidingPuzzle);

		JMenu helpMenu = new JMenu("Help");
		topMenuBar.add(helpMenu);

		JMenuItem aboutMenu = new JMenuItem("About me");
		aboutMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JDialog aboutMeDialog = new AboutMeDialog();
				aboutMeDialog.setAlwaysOnTop(true);
				aboutMeDialog.setModal(true);
				aboutMeDialog.setVisible(true);
			}
		});
		helpMenu.add(aboutMenu);
		frame.getContentPane().setLayout(
				new FlowLayout(FlowLayout.CENTER, 5, 5));

		mainPanel = new JPanel();
		frame.getContentPane().add(mainPanel);
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

		puzzlePanel = new JPanel();
		mainPanel.add(puzzlePanel);
		puzzlePanel.setLayout(new BoxLayout(puzzlePanel, BoxLayout.PAGE_AXIS));

	}

	public void reset() {
		flippingPuzzle = null;
		sliddingPuzzle = null;
		// frame.remove(puzzlePanel);
		// frame.removeAll();
		puzzlePanel.removeAll();
		frame.revalidate();
		frame.repaint();
		System.gc();
	}

	public FlippingPuzzle getFlippingPuzzle() {
		return flippingPuzzle;
	}

	public void setFlippingPuzzle(FlippingPuzzle flippingPuzzle) {
		this.flippingPuzzle = flippingPuzzle;
	}

	public SlidingPuzzle getSliddingPuzzle() {
		return sliddingPuzzle;
	}

	public void setSliddingPuzzle(SlidingPuzzle sliddingPuzzle) {
		this.sliddingPuzzle = sliddingPuzzle;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public JPanel getPanel() {
		return puzzlePanel;
	}

	public JPanel getPuzzlePanel() {
		return puzzlePanel;
	}

	public void setPuzzlePanel(JPanel puzzlePanel) {
		this.puzzlePanel = puzzlePanel;
	}

	public JFrame getFrame() {
		return frame;
	}
}
