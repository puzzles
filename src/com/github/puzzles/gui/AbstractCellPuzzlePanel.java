package com.github.puzzles.gui;

import javax.swing.JPanel;

abstract public class AbstractCellPuzzlePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5714650403244806247L;
	
	final private int xIndex;
	final private int yIndex;

	/**
	 * Create the panel.
	 */
	public AbstractCellPuzzlePanel(int xIndex, int yIndex) {
		this.xIndex = xIndex;
		this.yIndex = yIndex;
	}

	public int getXIndex() {
		return xIndex;
	}

	public int getYIndex() {
		return yIndex;
	}

}
