package com.github.puzzles.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.github.puzzles.core.SlidingPuzzle;

public class SlidingPuzzleDialog extends JDialog {

	/**
     * 
     */
	private static final long serialVersionUID = 4729972122941718936L;
	private final JPanel contentPanel = new JPanel();
	private JTextField rowsText;
	private JTextField colsText;
	private JLabel rowsNumberMessage;
	private JLabel rowsErrorMessage;
	private JLabel colsNumberMessage;
	private JLabel colsErrorMessage;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { try { FlippingPuzzleDialog
	 * dialog = new FlippingPuzzleDialog();
	 * dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	 * dialog.setVisible(true); } catch (Exception e) { e.printStackTrace(); } }
	 * //
	 */

	/**
	 * Create the dialog.
	 */
	public SlidingPuzzleDialog(final MainWindow mainWindow) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				rowsNumberMessage = new JLabel("Rows number");
				panel.add(rowsNumberMessage);
			}
			{
				rowsText = new JTextField();
				panel.add(rowsText);
				rowsText.setColumns(10);
			}
			{
				rowsErrorMessage = new JLabel("* should be a number.");
				rowsErrorMessage.setVisible(false);
				panel.add(rowsErrorMessage);
			}
		}

		JPanel panel = new JPanel();
		contentPanel.add(panel);
		{
			colsNumberMessage = new JLabel("Cols number");
			panel.add(colsNumberMessage);
		}
		{
			colsText = new JTextField();
			panel.add(colsText);
			colsText.setColumns(10);
		}
		{
			colsErrorMessage = new JLabel("* should be a number");
			colsErrorMessage.setVisible(false);
			panel.add(colsErrorMessage);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						int rows = 0;
						int cols = 0;
						boolean error = false;
						rowsErrorMessage.setVisible(false);
						colsErrorMessage.setVisible(false);
						try {
							rows = Integer.parseInt(rowsText.getText());
							error = true;
						} catch (NumberFormatException e1) {
							rowsErrorMessage.setVisible(true);
						}
						try {
							cols = Integer.parseInt(colsText.getText());
							error = true;
						} catch (NumberFormatException e1) {
							colsErrorMessage.setVisible(true);
						}

						if (error) {
							mainWindow.setSliddingPuzzle(new SlidingPuzzle(
									rows, cols));
							SlidingPuzzleDialog.this.dispose();
						}
						SlidingPuzzleDialog.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						SlidingPuzzleDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
