package com.github.puzzles.util;

import java.util.Arrays;

public class Matrices {

	// Don't let anyone instance this class.
	private Matrices() {
	}

	/**
	 * Copy 2 dimensions array.
	 * 
	 * @param original
	 *            the matrix which you want make a copy of it.
	 * @return the new copy.
	 */
	public static <T> T[][] copyOf(T[][] original, int colsLength,
			int rowsLength) {
		T[][] returnedPuzzle = Arrays.copyOf(original, colsLength);
		for (int i = 0; i < colsLength; i++) {
			returnedPuzzle[i] = Arrays.copyOf(original[i], rowsLength);
		}

		return returnedPuzzle;
	}

	public static <T> T[][] copyOf(T[][] original) {
		if (original.length < 0) {
			return null;
		}
		return copyOf(original, original.length, original[0].length);
	}

	public static <T> boolean equals(T[][] matrix, T[][] withMatrix)
			throws NullPointerException {
		if (matrix == withMatrix)
			return true;

		if (matrix == null && withMatrix == null)
			return true;

		if (matrix == null || withMatrix == null)
			return false;

		int width = getWidth(matrix);
		int height = getHeight(matrix);
		if (width != getWidth(withMatrix) && height != getHeight(withMatrix))
			return false;

		for (int i = 0; i < width; i++) {
			if (!Arrays.equals(matrix[i], withMatrix[i]))
				return false;
		}

		return true;
	}

	/**
	 * Fill a value in a matrix.
	 * 
	 * @param matrix
	 *            the matrix which you want to fill in it.
	 * @param val
	 *            the value which you want fill it in the matrix.
	 */
	public static <T> void fill(T[][] matrix, T val) {
		for (int i = 0; i < matrix.length; i++) {
			Arrays.fill(matrix[i], val);
		}
	}

	public static <T> int getWidth(T[][] matrix) {
		if (matrix != null) {
			return matrix.length;
		}
		return 0;
	}

	public static <T> int getHeight(T[][] matrix) {
		if ((getWidth(matrix) != 0) && (matrix[0] != null)) {
			return matrix[0].length;
		}
		return 0;
	}

	// Swap two elements with XOR swap algorithm.
	public static <T> void swap(T[][] matrix, int fromX, int fromY, int toX,
			int toY) {
		/*
		 * matrix[fromX][fromY] = matrix[fromX][fromY] ^ matrix[toX][toY];
		 * matrix[toX][toY] = matrix[fromX][fromY] ^ matrix[toX][toY];
		 * matrix[fromX][fromY] = matrix[fromX][fromY] ^ matrix[toX][toY]; //
		 */
		T temp = matrix[fromX][fromY];
		matrix[fromX][fromY] = matrix[toX][toY];
		matrix[toX][toY] = temp;
	}

}
