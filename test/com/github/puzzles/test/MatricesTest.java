package com.github.puzzles.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.github.puzzles.util.Matrices;

public class MatricesTest {

	public static void main(String[] argv) {

	}

	/*
	 * @Test public void swapTest() { SlidingPuzzle sp = new SlidingPuzzle(5,
	 * 5); Integer[][] puzzle = sp.getPuzzle(); Integer[] items = new
	 * Integer[25]; Arrays.fill(items, 0, 25, 0); for (int i = 0; i < 5; i++) {
	 * for (int j = 0; j < 5; j++) { items[puzzle[i][j]]++; } }
	 * 
	 * for (Integer item : items) { assertThat(item, is(1)); } } //
	 */

	@Test
	public void equalsTest() {
		Boolean[][] m1 = new Boolean[][] { { true, true, false },
				{ true, false, true } };
		Boolean[][] m2 = new Boolean[][] { { true, true, false },
				{ true, false, true } };

		assertThat(Matrices.equals(m1, m2), is(true));
	}

	@Test
	public void equalsTest2() {
		Boolean[][] m1 = new Boolean[][] { { true, true, false },
				{ true, false, false } };
		Boolean[][] m2 = new Boolean[][] { { true, true, false },
				{ true, false, true } };

		assertThat(Matrices.equals(m1, m2), is(false));
	}

	@Test
	public void equalsTest3() {
		Boolean[][] m1 = new Boolean[][] { { true, true, false },
				{ true, false, false } };

		assertThat(Matrices.equals(m1, m1), is(true));
	}

	@Test
	public void equalsTest4() {
		Boolean[][] m1 = null;
		Boolean[][] m2 = null;

		assertThat(Matrices.equals(m1, m2), is(true));
	}

	@Test
	public void equalsTest5() {
		Boolean[][] m1 = null;
		Boolean[][] m2 = new Boolean[][] { { true, true, false },
				{ true, false, true } };

		assertThat(Matrices.equals(m1, m2), is(false));
	}
}
