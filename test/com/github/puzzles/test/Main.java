package com.github.puzzles.test;

import com.github.puzzles.core.AbstractRectangularPuzzle;
import com.github.puzzles.core.Difficulty;
import com.github.puzzles.core.FlippingPuzzle;

public class Main {

	public static void main(String[] args) {
		FlippingPuzzle fp = new FlippingPuzzle(Difficulty.MEDUIM);
		printPuzzle(fp);
		System.out.println(fp.check());
	}

	public static void printPuzzle(AbstractRectangularPuzzle<Boolean> puzzle) {

		Boolean[][] puz = puzzle.getPuzzle();
		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz[i].length; j++) {
				System.out.print(((puz[i][j] == true) ? 1 : 0) + " ");
			}
			System.out.println();
		}
		System.out.println();

		/*
		 * for(Boolean[] line : puzzle.getPuzzle()){ for(Boolean block : line)
		 * System.out.print(block); System.out.println(); } //
		 */
	}
}
