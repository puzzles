package com.github.puzzles.test;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.github.puzzles.core.Difficulty;
import com.github.puzzles.core.FlippingPuzzle;
import com.github.puzzles.core.PersonalisedDifficultyException;

public class FlipPuzzleTest {

	@Test
	public void testCheck() {
		FlippingPuzzle fp = new FlippingPuzzle(Difficulty.MEDUIM);
		fp.flip(3, 4);
	}

	@Test
	public void testFlipPuzzleBooleanArrayArray() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlipPuzzleIntInt() {
		fail("Not yet implemented");
	}

	@Test(expected = PersonalisedDifficultyException.class)
	public void testFlipPuzzleDifficulty() {
		new FlippingPuzzle(Difficulty.PERSONALISED);
	}

	@Test
	public void testFlip() {
		fail("Not yet implemented");
	}

	@Test
	public void getPuzzle() {

	}
	
	@Test
	public void willFlipTest(){
		FlippingPuzzle fp = new FlippingPuzzle(5, 5);
		fp.flip(2, 2);
		for(Boolean bool : fp.getWillFlip(2, 2))
			System.out.println(bool);
	}
}
